import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CustomerActivatedComponent } from './customer-activated.component';

describe('CustomerActivatedComponent', () => {
  let component: CustomerActivatedComponent;
  let fixture: ComponentFixture<CustomerActivatedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CustomerActivatedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CustomerActivatedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
