export interface Customer {
	key?: string,
	pin : string,
	tel : string,
	cin : string,
	cr  : string,
	name : string,
	date : string
}